import java.util.*;
public class DeadFish {
    public static int[] parse(String data) {
    
       int res = 0;
       
       List<Integer> listRes = new ArrayList<Integer>();
       StringBuilder str = new StringBuilder();
       str.append(data);
       
       for(int i = 0; i < data.length(); i++){
       
       switch(str.charAt(i)){
       
       case 'i':
       res += 1;
       break;
       case 'd':
       res -= 1;
       break;
       case 's':
       res *= res;
       break;
       default:
       listRes.add(res);
       
         }
       }
       
       return listRes.stream().mapToInt(l -> l).toArray();
    }
}
