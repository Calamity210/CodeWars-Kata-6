public class SexagesimalDegree {
  
 public static String convert(double lat, double lon) {
    // Your code
    System.out.println(lat +"::::" + lon);
    String dirLat = lat > 0 ? "N" : "S";
    String dirLon = lon > 0 ? "E" : "W";
    
    String res = Math.abs((int)lat) +"° ";
    
    double newLat = lat - (int)lat;
    double newLon = lon - (int)lon;
    
    
    System.out.println(newLat +"::::" + newLon);
    
    double minutesLat = newLat* 60;
    double secondsLat = Math.round((minutesLat - (int)minutesLat) * 60);
    
    System.out.println(minutesLat +"::::" + secondsLat);
    
    double minutesLon = newLon * 60;
    double secondsLon = Math.round((minutesLon - (int)minutesLon) * 60);
    
    
    System.out.println(minutesLon +"::::" + secondsLon);
    
    int minLat = Math.abs((int)minutesLat);
    
    int secLat = Math.abs((int)secondsLat);
    
    int minLon = Math.abs((int)minutesLon);
    
    int secLon = Math.abs((int)secondsLon);
    
    res += minLat + "′ " + secLat + "″ " + dirLat+", " + Math.abs((int)lon) +"° "+minLon+"′ "+secLon+"″ "+dirLon;
    return res;
    
    
}
}
