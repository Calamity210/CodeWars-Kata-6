import java.util.*;

public class UserSolution {
  //still needs optimization for long inputs, but good starting point
  
  public static long[] oddRow(int n) {
  List<Long> result = new ArrayList<Long>();
  long g = Long.valueOf(n);
  long firstNum = (g * (g-1)) + 1;
  result.add(Long.valueOf( firstNum) );
  for(int i = 1; i < n; i++){
  result.add(Long.valueOf( firstNum + (2 *i) ));
  }
  return result.stream().mapToLong(l -> l).toArray();
